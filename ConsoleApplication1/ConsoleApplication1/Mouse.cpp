#include "Mouse.h"
#include <iostream>
using namespace std;

Mouse::Mouse(const char nameParam[], int dpi) : Periferice(nameParam)
{
	strcpy_s(name, nameParam);
	this->dpi = dpi;
}

void Mouse::ConsoleWrite()
{
	cout << "Mouse " << name << " cu " << dpi << " dpi\n";
}

Mouse::Mouse()
{
	strcpy_s(name, "Generic");
	this->dpi = 400;
}


Mouse::~Mouse()
{
}

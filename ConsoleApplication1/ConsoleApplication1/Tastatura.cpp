#include "Tastatura.h"
#include <iostream>
using namespace std;



Tastatura::Tastatura(const char nameParam[], const char typeParam[]) : Periferice(nameParam)
{
	strcpy_s(name, nameParam);
	strcpy_s(type, typeParam);
}

void Tastatura::ConsoleWrite()
{
	cout << "Tastatura " << name << " de tip " << type << endl;
}

Tastatura::Tastatura()
{
	strcpy_s(name, "Generica");
	strcpy_s(type, "General");
}


Tastatura::~Tastatura()
{
}

#pragma once
class HasConsoleOperations
{
public:
	virtual void ConsoleWrite() = 0;
	~HasConsoleOperations();
};


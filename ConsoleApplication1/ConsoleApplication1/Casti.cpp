#include "Casti.h"
#include <iostream>
using namespace std;


Casti::Casti(const char nameParam[], int dB) : Periferice(nameParam)
{
	strcpy_s(name, nameParam);
	db = dB;
}

void Casti::ConsoleWrite()
{
	cout << "Casti " << name << " cu " << db << " dB \n";
}

Casti::Casti()
{
	strcpy_s(name, "Generice");
	db = 0;
}


Casti::~Casti()
{
}

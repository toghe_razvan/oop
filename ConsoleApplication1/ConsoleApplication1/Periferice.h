#pragma once
#include "HasConsoleOperations.h"
class Periferice:HasConsoleOperations
{
protected:
	char name[20];
public:
	Periferice(const char nameParam[]);
	virtual void ConsoleWrite();
	Periferice();
	~Periferice();
};


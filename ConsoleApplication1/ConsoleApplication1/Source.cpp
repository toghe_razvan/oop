#include <iostream>
#include "Mouse.h"
#include "Casti.h"
#include "Tastatura.h"
using namespace std;

int main() {
	Mouse m1{"Razer", 4800}, m2;
	Tastatura t1{ "Genessis", "Mecanica" }, t2;
	Casti c1{ "Razer",98 }, c2;
	Periferice* P1[3] = { &m1, &c1, &t1}, *P2[3] = { &m2, &c2, &t2 };
	P1[0]->ConsoleWrite();
	P1[1]->ConsoleWrite();
	P1[2]->ConsoleWrite();
	cout << endl;
	P2[0]->ConsoleWrite();
	P2[1]->ConsoleWrite();
	P2[2]->ConsoleWrite();
}